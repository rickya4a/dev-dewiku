<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function apps()
    {
        return $this->hasMany(UserApp::class, 'users_id');
    }

    public function appType()
    {
        return $this->belongsTo(UserApp::class, 'id', 'users_id');
    }

    public function scopeApp($query, $appName = '')
    {
        if (Auth::user()->level == 1 || Auth::user()->level == 3) {
            if ($appName != '')
                return $query->whereIn('level', [1,3])->orWhereHas('apps', function($subquery) use($appName){
                    $subquery->where('app_name', $appName);
                });

            return $query;
        }
        return $query->whereHas('apps', function($subquery) use($appName) {
            $subquery->where('app_name', $appName);
        });
    }
}
