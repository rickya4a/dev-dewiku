<?php

namespace App;

class Helpers
{
    // public static function platform()
    // {
    //     $agent = new \Jenssegers\Agent\Agent;

    //     $platform = "mobile";
    //     if($agent->isDesktop()){
    //         $platform = "desktop";
    //     }

    //     return $platform;
    // }
    public static function isLocal(){
        switch (config('general.appEnv')) {
            case 'local':
                $result = true;
                break;

            default:
                $result = false;
                break;
        }
        return $result;
    }
    public static function siteName()
    {
        $siteName = \App\Lib\ClientLib::get(['app_name'=>self::appName()])->title??'';

        return $siteName;
    }
    public static function appName()
    {
        try {
            $appName = request()->session()->get('app_name')??'unknown';
        } catch (\Throwable $th) {
            $appName = '';
        }

        return $appName;
    }
    public static function categories()
    {
        $appName = config('partner.clients.'.self::appName().'.categories')??array();

        return $appName;
    }
    public static function mainUrl($path = '')
    {
        $mainUrl = config('general.mainUrl');

        return trim($mainUrl,'/') . $path;
    }
    public static function assetUrl($path, $partnerAsset = false)
    {
        if(!$partnerAsset){
            $assetUrl = config('general.assetUrl');
        }else{
            $assetUrl = config('general.partnerAssetUrl');
        }

        return trim($assetUrl,'/') . '/' . trim($path,"/");
    }
    public static function thumbImage(string $path, int $w = 745, int $h = 489, string $appName)
    {
        $mediaUrl = config('general.mediaUrl');
        $width = $w;
        // $height = floor($r * $width);
        $height = $h;
        if(self::appName() != ''){
            $appName = self::appName();
        }
        $newPath = trim($mediaUrl,'/').'/'.'mitra'.'/'.$appName.'/' . 'thumbs/' . $width .'x'.$height .'/'.$path;

        return $newPath;
    }
    public static function getReadUrl($postData)
    {
        $config = config('general');
        $readUrl = trim($config['mainUrl'],'/')
        . '/read' .'/' . $postData['category']['slug'] . '/'
        . $postData['postId'] . '/' . $postData['slug'];
        return $readUrl;
    }
    public static function getReadUrlAmp($postData)
    {
        $config = config('general');
        $readUrl = trim($config['mainUrl'],'/')
            . '/amp/read' .'/' . $postData['category']['slug'] . '/'
            . $postData['postId'] . '/' . $postData['slug'];
        return $readUrl;
    }
    public static function getCategoryUrl($category)
    {
        $config = config('general');
        return trim($config['mainUrl'],'/') . "/" . $category['slug'];
    }
    public static function getTagUrl($tag)
    {
        $config = config('general');
        return trim($config['mainUrl'],'/') . "/tag" . "/" . $tag;
    }
    public static function appendHTML($parent, $source) {
        $tmpDoc = new \DOMDocument();
        $tmpDoc->loadHTML($source);
        foreach ($tmpDoc->getElementsByTagName('body')->item(0)->childNodes as $node) {
            $node = $parent->ownerDocument->importNode($node, true);
            $parent->appendChild($node);
        }
    }
    public static function dateLangReformat($sttime, $lang = "id", $useday = true, $usetime = false)
    {
        $date_day_id = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
        $date_month_id = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

        $time = "";
        $aday = "";
        if($usetime){
            $time = " | ".date("H:i",$sttime)." WIB";
        }
        if($useday){
            $aday = $date_day_id[date("w", $sttime)].", ";
        }
        if ($lang == "id") {
            return $aday.date("j", $sttime)." ".$date_month_id[date("n", $sttime)-1]." ".date("Y", $sttime).$time;
        }
        elseif($lang == "id-short"){
            return $aday.date("j", $sttime)." ".substr($date_month_id[date("n", $sttime)-1],0,3)." ".date("Y", $sttime).$time;
        }
        else {
            return date("l", $sttime).", ".date("F", $sttime)." ".date("j", $sttime)." ".date("Y", $sttime).$time;
        }
    }
    public static function random_words($words = 1, $length = 6)
    {
        $string = '';
        for ($o=1; $o <= $words; $o++)
        {
            $vowels = array("a","e","i","o","u","4","3","1","0");
            $consonantLow = array(
                'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm',
                'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'
            );
            foreach ($consonantLow as $key => $value) {
                $consonantCap[] = strtoupper($value);
            }

            $consonants = array_merge($consonantLow, $consonantCap);


            $word = '';
            for ($i = 1; $i <= $length; $i++)
            {
                $word .= $consonants[rand(0,39)];
                $word .= $vowels[rand(0,8)];
            }
            $string .= mb_substr($word, 0, $length);
            $string .= "-";
        }
        return mb_substr($string, 0, -1);
    }

}
