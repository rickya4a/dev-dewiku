<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Content;
use Illuminate\Support\Str;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Content::all();
        // dd($posts);
        return Inertia::render('Content/List', [
            'data' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $inertiaData['categories'] = \App\Lib\CategoryLib::getAll();
        $inertiaData['topic'] = \App\Lib\SuaraLib::getTopic();
        $inertiaData['flags'] = config('general.flags');
        $inertiaData['authors'] = \App\Models\User::app(\App\Helpers::appName())->where('status', 1)->get();

        return Inertia::render('Content/Create', $inertiaData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request->validate([
            'content.title' => 'required|min:20',
            'content.summary' => 'required|min:20',
            'content.mainBody' => 'required|min:100',
            'property.image' => 'required',
            'property.image.og_filename' => 'different:property.image.title',
            'property.category' => 'required',
            // 'property.topic' => 'required',
            'property.tags' => 'required',
            'property.related' => 'required',
            'property.lainnya' => 'required',
        ], [
            'content.title.required' => 'Title is not detected',
            'content.title.min' => 'Title is too short',

            'content.summary.required' => 'Summary is not detected',
            'content.summary.min' => 'Summary is too short',

            'content.mainBody.required' => 'Content is not detected',
            'content.mainBody.min' => 'Content is too short',

            'property.image.required' => 'Featured Image is not set',
            'property.image.og_filename.different' => 'Featured Image title must different with original file name',
            'property.category.required' => 'Category is not set',
            'property.topic.required' => 'Topic is not set',
            'property.tags.required' => 'Tags is not set',
            'property.related.required' => 'Berita Terkait is not set',
            'property.lainnya.required' => 'Baca Juga is not set',
        ]); */

        $post = Content::create([
            'type' => $request->input('property')['postType'],
            'title' => $request->input('content')['title'],
            'slug' => Str::slug($request->input('content')['title']),
            'summary' => $request->input('content')['summary']
        ]);

        if ($post) return redirect()->route('content-list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $post = Content::where('id', $id)->first();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 1);
        }

        return Inertia::render('Content/Create', [
            'post' => ['content' => $post]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $post = Content::where('id', $request->id)->update([
            'type' => $request->input('property')['postType'],
            'title' => $request->input('content')['title'],
            'slug' => Str::slug($request->input('content')['title']),
            'summary' => $request->input('content')['summary']
        ]);

        if ($post) return redirect()->route('content-list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
