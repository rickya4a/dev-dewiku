<?php

namespace App\Lib;

use Cache;
class ClientLib
{
    public static function list()
    {
        $cacheKey = "clientLib:list";
        $cacheTimeout = 60;

        $clients = Cache::remember($cacheKey, $cacheTimeout, function (){
            $clients = \App\Models\Apps::where('status', 1)->get();

            return $clients->toArray();
        });

        return $clients;
    }

    public static function get($params = array()){

        $cacheKey = "clientLib:get:".base64_encode(json_encode($params));
        $cacheTimeout = 1;

        $clients = Cache::remember($cacheKey, $cacheTimeout, function () use($params) {
            $clients = \App\Models\Apps::where('status', 1);

            if(isset($params['app_name'])){
                $clients = $clients->where('app_name', $params['app_name'])->first();
            }
            return $clients;
        });
        return $clients;
    }
}
