<?php

namespace App\Lib;

use Http;
use Cache;
/**
 * Suara Lib
 * by Hitori AF
 */
class SuaraLib
{
    public static function getArticles($params)
    {
        $result = Cache::remember('dewikulib-getarticles:'.base64_encode(json_encode($params)), 1, function() use($params){
            $result = Http::get('https://melon.matamata.com/content/lainnya/lists', $params)->json();
            $result = collect($result);
            return $result;
        });
        return $result;
    }

    public static function getTopic($params=[])
    {
        $result = Cache::remember('dewikulib-getTopic:'.base64_encode(json_encode($params)), 1, function() use($params){
            $result = Http::get('https://melon.matamata.com/content/topic/list', $params)->json();
            $result = collect($result);
            return json_decode($result, TRUE);
        });

        return $result;
    }
}
