<?php 
/**
 * LIBRARY HANYA UNTUK KEPENTINGAN BACKEND CMS
 */

namespace App\Lib;

class CategoryLib {
    
    public static function getAll(){
        $cats = \App\Models\Categories::query();
        $cats = $cats->where('app_name', \App\Helpers::appName());
        $cats = $cats->where('status', 1)->orderBy('created_at','desc')->get();
        if($cats){
            $cats = $cats->toArray();
        }
        $categories = [];
        foreach ($cats as $key => $cat) {
            $categories[$key]['id'] = $cat['id'];
            $categories[$key]['name'] = $cat['name'];
            $categories[$key]['slug'] = $cat['slug'];
            $categories[$key]['parentId'] = $cat['parent_id']??0;
            $categories[$key]['suaraCatId'] = $cat['suara_cat_id']??0;
            $categories[$key]['childs'] = $cat['childs'];
        }
        return $categories;
        // $catLib = new CategoryLib;
        // $catTree = $catLib->buildTree($categories);
        // dd($catLib->normalizeTree($catTree));
        // return $catLib->normalizeTree($catTree);


    }
    private function normalizeTree($tree, $n=0, $result = [], $p = 0){
        foreach ($tree as $k => $v) {
            $result[$n] = $v;
            if($p > 0){
                $slasher = '';
                for ($x=0; $x < $p; $x++) { 
                    $slasher .= "-";
                }
                $result[$n]['name'] = $slasher. ' '. $v['name'];
            }
            if(isset($v['childs'])){
                $n++;
                $result = $this->normalizeTree($v['childs'], $n++, $result, $p + 1);
            }else{

                $n++;
            }
        }
        return $result;
    }
    private function buildTree($items) {
        $childs = array();
    
        foreach($items as &$item) $childs[(int)$item['parentId']][] = &$item;
    
        foreach($items as &$item) if (isset($childs[$item['id']]))
                $item['childs'] = $childs[$item['id']];
       
        return isset($childs[0])?$childs[0] : []; // Root only.
    }
}