<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('type')->nullable();
            $table->string('title')->unique();
            $table->string('slug')->unique();
            $table->text('body')->change();
            $table->string('keywords')->nullable();
            $table->string('tags')->nullable();
            $table->json('sections')->nullable();
            $table->string('image_source')->nullable();
            $table->mediumText('summary')->nullable();
            $table->json('video')->nullable();
            $table->json('flags')->nullable();
            $table->string('app_name')->nullable();
            $table->integer('suara_pid')->nullable();
            $table->json('lainnya')->nullable();
            $table->json('related')->nullable();
            $table->dateTime('published_at', $precision = 0)->nullable();
            $table->integer('image_id')->nullable();
            $table->string('image_title')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('author_id')->nullable();
            $table->string('author_alias')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
};
