<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth', 'verified'])->prefix('dewiku')->group(function () {
    // Route::get('/admin-dashboard', [App\Http\Controllers\ContentController::class, 'index'])->name('admin.dashboard');
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::prefix('content')->group(function(){
        Route::get('/', [\App\Http\Controllers\ContentController::class, 'index'])->name('content-list');
        Route::get('/create', [\App\Http\Controllers\ContentController::class, 'create'])->name('content-create');
        Route::get('/edit/{id}', [\App\Http\Controllers\ContentController::class, 'edit'])->name('content-edit');
        Route::post('/publish', [\App\Http\Controllers\ContentController::class, 'store'])->name('content-publish');
        Route::post('/update', [\App\Http\Controllers\ContentController::class, 'update'])->name('content-update');
    });
});


require __DIR__.'/auth.php';
